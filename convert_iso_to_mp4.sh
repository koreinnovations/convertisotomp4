#!/bin/bash
MOUNTPOINT=/Volumes/_automated_conversion
hdiutil mount $1 -mountpoint $MOUNTPOINT
cat ${MOUNTPOINT}/VIDEO_TS/VTS*.VOB | ffmpeg -i - $2
hdiutil unmount $MOUNTPOINT