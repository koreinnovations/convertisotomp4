# Usage Instructions

## Requirements

1. Install homebrew (http://mxcl.github.io/homebrew/)
1. Install FFMpeg using brew  
`brew install ffmpeg`
1. Make the script writable  
`chmod a+x convert_iso_to_mp4.sh`

## Script Usage

`./convert_iso_to_mp4.sh PATH_TO_ISO_FILE PATH_TO_MP4_OUTPUT_FILE`

Above is the command to run the script when you are in the script's working directory.

`PATH_TO_ISO_FILE` is the path to the existing ISO file, either across the network or on the local system.

`PATH_TO_MP4_OUTPUT_FILE` is the path to the MP4 file that you want to create and save through the conversion process.

